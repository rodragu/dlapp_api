<?php
/**
 * Created by PhpStorm.
 * User: rodrigoaguna
 * Date: 9/9/15
 * Time: 8:35
 */

namespace api\rest\components;

class wChannel
{
    const USSD = 0;
    const WEB = 1;
    const AGENT = 2;
    const APP = 3;
}