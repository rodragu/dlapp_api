<?php

namespace api\rest\components;

class wChannel
{
    const USSD = 0;
    const WEB = 1;
    const AGENT = 2;
    const APP = 3;
}

class wPaymentStatus
{
    const PLACED = 0;
    const AUTHORIZED = 1;
    const PAID = 2;
    const VOID = 3;
    const SENT = 4;
    const AUTHORISED = 5;
}

class wPaymentOperator
{
    const MBET = 0;
    const VODACOM = 1;
    const TIGO = 2;
    const AIRTEL = 3;
    const PAYPAL = 4;

    public static function getPayOpName($id)
    {
        $op = array(
            self::MBET => 'M-Bet',
            self::VODACOM => wVodacom::NAME,
            self::TIGO => wTigo::NAME
        );
        return !is_null($id) ? $op[$id] : null;
    }

    public static function getBusNo($id)
    {
        $op = array(
            self::VODACOM => wVodacom::BUSINESS_NO,
            self::TIGO => wTigo::BUSINESS_NO
        );
        return !is_null($id) ? $op[$id] : null;
    }

    public static function getShortCode($id)
    {
        $op = array(
            self::VODACOM => wVodacom::SHORTCODE,
            self::TIGO => wTigo::SHORTCODE
        );
        return !is_null($id) ? $op[$id] : null;
    }
}

class wVodacom
{
    const ID = wPaymentOperator::VODACOM;
    const NAME = 'M-Pesa';
    const SHORTCODE = '*150*00#';
    const BUSINESS_NO = '300300';
}

class wTigo
{
    const ID = wPaymentOperator::TIGO;
    const NAME = 'Tigo Pesa';
    const SHORTCODE = '*150*01#';
    const BUSINESS_NO = '300300';
}