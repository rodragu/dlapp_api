<?php

namespace rest\versions\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use common\models\Situation;
use yii\base\Exception;


/**
 * Class UserController
 * @package rest\versions\v1\controllers
 */
class SituationController extends ActiveController
{
    public $modelClass = 'common\models\Situation';

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Añade una situación de queja
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - dev_key
    // - access_token
    // - situation
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json con el código 0 si todo es correcto o sino con el código de error correspondiente
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionAdd_situation()
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost) {
                $key_dev = $request->getBodyParam('dev_key');

                if ($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $situation = $request->getBodyParam('situation');

                    $situ = new Situation();
                    $situ->type = $situation;

                    if($situ->validate())
                    {
                        $situ->save();
                    }
                    else
                    {

                        return $serializer = [
                            'status' => '3',
                            'status_message' => 'KO',
                            'errors' => $situ->errors
                        ];
                    }

                    return $serializer = [
                        'status' => '0',
                        'status_message' => 'OK',
                        'description_message' => 'Situation saved'
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        } catch (Exception $e) {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = ' . $e,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Obtiene una situación de queja
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - dev_key
    // - access_token
    // - situation
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json con el código 0 si todo es correcto o sino con el código de error correspondiente
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionGet_situation()
    {
        try {
            $request = \Yii::$app->request;

            if ($request->isPost) {
                $key_dev = $request->getBodyParam('dev_key');

                if ($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $situ = $request->getBodyParam('situation_id');
                    $situation = Situation::findOne(['id' => $situ]);

                    if($situ)
                    {
                        return $serializer = [
                            'status' => '0',
                            'status_message' => 'OK',
                            'situation' => $situation
                        ];
                    }
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        } catch (Exception $e) {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = ' . $e,
            ];
        }
    }
}