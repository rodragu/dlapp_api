<?php
namespace rest\versions\v1\controllers;

use Yii;
use yii\rest\Controller;
use common\models\User;
use yii\base\Exception;


/**
 * Class UserController
 * @package rest\versions\v1\controllers
 */
class UserController extends Controller
{
    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Función que obtiene el perfil del usuario con su operadora.
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - access_token
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json user
    //  - operator
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionGet_profile()
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
//                    $so = $request->getBodyParam('so');
//                    $app_version = $request->getBodyParam('app_version');

                    $token = $request->getBodyParam('access_token');

                    //Consultamos el nombre pasado en la request y lo guardamos en $user

                    $user = User::findIdentityByAccessToken($token);

                    if ($user)
                    {
                        return $serializer = [
                            'status' => '0',
                            'status_message' => 'OK',
                            'description_message' => 'OK',
                            'user' => $user,
                        ];
                    }
                    else
                    {
                        return $serializer = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'No User found',
                        ];
                    }
                }
                else
                {
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    // - Función que cambia la contraseña de un usuario.
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - 'old_password',
    // - 'new_password',
    // - 'access_token'
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    // - Mensaje 'Password saved'.
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionChange_password()
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
//                    $so = $request->getBodyParam('so');
//                    $app_version = $request->getBodyParam('app_version');

                    $old_password = $request->getBodyParam('old_password');
                    $new_password = $request->getBodyParam('new_password');
                    $token = $request->getBodyParam('access_token');

                    $user = User::findIdentityByAccessToken($token);

                    if ($user)
                    {
                        if ($user->validatePassword($old_password))
                        {
                            if (isset($new_password))
                            {
                                $user->setPassword($new_password);
                                $user->access_token = NULL;
                                $user->update();

                                return $serializer = [
                                    'status' => '0',
                                    'status_message' => 'OK',
                                    'description_message' => 'Password saved'
                                    ];
                            }
                            else
                            {
                                return $serializer = [
                                    'status' => '6',
                                    'status_message' => 'KO',
                                    'description_message' => 'New Password missed'
                                ];
                            }
                        }
                        else
                        {
                            return $serializer = [
                                'status' => '5',
                                'status_message' => 'KO',
                                'description_message' => 'Incorrect Password',
                            ];
                        }
                    }
                    else
                    {
                        return $serializer = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'No User found',
                        ];
                    }
                }
                else
                {
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    // - Función que sirve para registrar un usuario nuevo.
    //
    //* PARÁMETROS (Pasados desde la aplicación) ------------------------------------------------------------
    //
    // - dev_key
    // - first_name
    // - last_name
    // - phone
    // - operator_id
    // - address
    // - city
    // - country
    // - email
    // - username
    // - password
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    // - Nos devuelve un Access_token para que el usuario ya esté logado.
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionAdd_user()
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $username = $request->getBodyParam('username');

                    $user = User::findByUsername($username);

                    if(is_null($user))
                    {
                        $user = new User(['scenario' => 'register']);

                        $user->username = $request->getBodyParam('username');
                        $password = $request->getBodyParam('password');
                        $user->setPassword($password);
                        $user->email = $request->getBodyParam('email');

                        if ($user->validate())
                        {
                            // all inputs are valid
                            $user->save();

                            return $this->actionLogin2($user->username);
                        }
                        else
                        {
                            // validation failed: $errors is an array containing error messages
                            $errors = $user->errors;

                            return $serializer = [
                                'status' => '5',
                                'status_message' => 'KO',
                                'description_message' => 'Validation errors',
                                'Errors' => $errors,
                            ];
                        }
                    }
                    else
                    {
                        return $serializer = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'User already registered',
                        ];
                    }
                }
                else
                {
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
            'status' => '1',
            'status_message' => 'KO',
            'description_message' => 'KO exception = '.$e,
            ];
        }
    }

    public function actionLogin()
    {
        //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
        //
        //  - Función que loguea un usuario en la aplicación.
        //
        //* PARÁMETROS (Pasados desde la aplicación) ------------------------------------------------------------
        //
        // - mbet_dev_key
        // - so
        // - app_version
        // - maintenance
        // - username
        // - password
        //
        //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
        //
        //  - Access_token: Hash que identifica al usuario.
        //  - User: Array Json con todos los datos de este usuario.
        //
        // ------------------------------------------------------------------------------------------------------

        $maintenance = \Yii::$app->params['maintenance'];

        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $so = $request->getBodyParam('so');
                    $app_version = $request->getBodyParam('app_version');
                    $country = $request->getBodyParam('country');
                    $username = $request->getBodyParam('username');
                    $password = $request->getBodyParam('password');

                    //Consultamos el nombre pasado en la request y lo guardamos en $user

                    $user = User::findByUsername($username);

                    if ($user)
                    {
                        $user->scenario = USER::SCENARIO_LOGIN;

                        if ($user->validatePassword($password))
                        {
                            if(is_null($user->access_token))
                            {
                                return $serializer = [
                                    'status' => '6',
                                    'status_message' => 'KO',
                                    'description_message' => 'User has no Access_Token',
                                    'maintenance' => $maintenance,
                                ];
                            }

                            return $serializer = [
                                'status' => '0',
                                'status_message' => 'OK',
                                'description_message' => 'OK',
                                'access_token' => $user->access_token,
                                'user' => $user,
                                'maintenance' => $maintenance,
                            ];
                        }
                        else
                        {
                            $errors = $user->errors;

                            return $serializer = [
                                'status' => '5',
                                'status_message' => 'KO',
                                'description_message' => 'Incorrect Password.',
                                'maintenance' => $maintenance,
                            ];
                        }
                    }
                    else
                    {
                        return $serializer = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'No User found',
                            'maintenance' => $maintenance,
                        ];
                    }
                }
                else
                {
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                        'maintenance' => $maintenance,
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                    'maintenance' => $maintenance,
                ];
            }
        }
        catch (Exception $e){
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
//                'maintenance' => $maintenance,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Función que loguea un usuario en la aplicación.
    //
    //* PARÁMETROS (Pasados directamente a la función) ------------------------------------------------------------
    //
    //  - Username
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Access_token: Hash que identifica al usuario.
    //  - User: Array Json con todos los datos de este usuario.
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionLogin2($username)
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $so = $request->getBodyParam('so');
//                    $app_version = $request->getBodyParam('app_version');

                    //Consultamos el nombre pasado en la request y lo guardamos en $user

                    $user = User::findByUsername($username);

                    if ($user)
                    {
                        $user->scenario = USER::SCENARIO_LOGIN;

                        //si entra tenemos que obtener el access_token

                        // Crear un nuevo recurso cURL
                        $ch = curl_init();

                        // Establecer URL y otras opciones apropiadas
//                        curl_setopt($ch, CURLOPT_URL, \Yii::$app->params['URL_Login']);
                        curl_setopt($ch, CURLOPT_URL, \Yii::$app->params['URL_Login']);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                            'client_id' => 'testclient',
                            'client_secret' => 'testpass',
                            'grant_type' => 'client_credentials',
                            'username' => $user->username,
                            'password' => $user->password,
                            //'user_id' => $user->id,
                            //'redirect_uri' => '',
                            //'code' => $_GET['code'], // The code from the previous request
                        ));

                        // Capturar la URL y pasarla al navegador
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
//                        curl_setopt($ch, CURLOPT_PORT, 80); //Set the port number

                        $token = curl_exec($ch);
                        var_dump($token);
                        error_log("Token en bruto ". $token);
                        $tk = json_decode($token);

                        if($user->access_token == NULL)
                        {
                            error_log("tk->access_token ". $tk->access_token);
                            $user->access_token = $tk->access_token;
                            $user->save();
                        }

                        // Cerrar el recurso cURL y liberar recursos del sistema
                        curl_close($ch);

                        return $serializer = [
                            'status' => '0',
                            'status_message' => 'OK',
                            'description_message' => 'OK',
                            'token' => $user->access_token,
                            'user' => $user
                        ];
                    }
                    else{
                        return $serializer = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'No User found',
                        ];
                    }
                }else{
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e){
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }


    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Función que modifica y actualiza los campos de un usuario.
    //
    //* PARÁMETROS (Pasados desde la aplicación) ------------------------------------------------------------
    //
    //  - Se le pasará un listado de los campos más importantes que el usuario puede modificar desde la aplicación.
    //
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json con los datos del usuario.
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionUser_update()
    {
        try {

            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $phone = $request->getBodyParam('phone');

                    $user = User::findByUsername($phone);

                    if ($user)
                    {
                        $user->scenario = 'update';

                        $user->username = $request->getBodyParam('username');
                        $user->password = $request->getBodyParam('password');
                        $user->email = $request->getBodyParam('email');

                        if ($user->validate()) {
                            // all inputs are valid
                            $user->update();

                            return $serialize = [
                                'status' => '0',
                                'status_message' => 'OK',
                                'description_message' => 'OK',
                                'User' => $user
                            ];
                        }
                        else
                        {
                            // validation failed: $errors is an array containing error messages
                            $errors = $user->errors;

                            return $serialize = [
                                'status' => '5',
                                'status_message' => 'KO',
                                'description_message' => 'Validation errors',
                                'Errors' => $errors,
                            ];
                        }
                    }
                    else
                    {
                        return $serialize = [
                            'status' => '4',
                            'status_message' => 'KO',
                            'description_message' => 'User not found',
                        ];
                    }
                }
                else
                {
                    return $serializer = [
                        'status' => '3',
                        'status_message' => 'KO',
                        'description_message' => 'No error',
                    ];
                }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e){
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }
}
