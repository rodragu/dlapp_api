<?php

namespace rest\versions\v1\controllers;

use common\models\Complaint;
use common\models\Situation;
use common\models\User;
use Yii;
use yii\base\Exception;
use yii\rest\ActiveController;


/**
 * Class UserController
 * @package rest\versions\v1\controllers
 */
class ComplaintController extends ActiveController
{
    public $modelClass = 'common\models\Complaint';

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  -
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - dev_key
    // - access_token
    // - labor_union_id
    // - visit
    // - info
    // - venue
    // - situation_id
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionAdd_complaint()
    {
        try {
            $request = \Yii::$app->request;

            if ($request->isPost) {
                $key_dev = $request->getBodyParam('dev_key');

                if ($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $token = $request->Post('access_token');

                    $user = User::findIdentityByAccessToken($token);

                    if($user)
                    {
                        $denuncia = new Complaint();

                        $denuncia->attributes = \Yii::$app->request->post();
                        $denuncia->user_id = $user->id;
                        $denuncia->save();

                        return $serializer = [
                            'status' => '0',
                            'status_message' => 'OK',
                        ];
                    }
                    else
                    {

                    }

                }
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Obtiene una Complaint mediante el id de ella
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - dev_key
    // - access_token
    // - complaint_id
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionGet_complaint()
    {
        try {
            $request = \Yii::$app->request;

            if ($request->isPost) {
                $key_dev = $request->getBodyParam('dev_key');

                if ($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $compl_id = $request->getBodyParam('complaint_id');

                    $complaint = Complaint::findOne(['id' => $compl_id]);

                    if($complaint)
                    {
                        return $serializer = [
                            'status' => '0',
                            'status_message' => 'OK',
                            'complaint' => $complaint
                        ];
                    }
                    else
                    {
                        return $serializer = [
                            'status' => '3',
                            'status_message' => 'KO',
                            'description_message' => 'Complaint not found'
                        ];
                    }
                }
            }
            else {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }

    //* DESCRIPCIÓN -----------------------------------------------------------------------------------------
    //
    //  - Obtiene las Complaints dependiendo de los parámetros que se le pasen (complaints de usuario o de situación)
    //
    //* PARÁMETROS (Pasados desde la aplicación)    ---------------------------------------------------------
    //
    // - dev_key
    // - access_token o situation_id
    //
    //* DATOS DEVUELTOS -------------------------------------------------------------------------------------
    //
    //  - Json
    //
    // ------------------------------------------------------------------------------------------------------

    public function actionGet_complaints()
    {
        try
        {
            $request = \Yii::$app->request;

            if ($request->isPost)
            {
                $key_dev = $request->getBodyParam('dev_key');

                if ($key_dev == "Dw8;Nq#I1Zc)q[NJQb#__{{a__=v~cH=KU{hf@{Z!^)}?Hk")
                {
                    $token = $request->getBodyParam('access_token');
                    $situ_id = $request->getBodyParam('situation_id');

                    $user = User::findIdentityByAccessToken($token);

                    if(is_null($token) && is_null($user))
                    {
                        return $serializer = [
                            'status' => '3',
                            'status_message' => 'KO',
                            'description_message' => 'Parameters not found'
                        ];
                    }
                    else
                    {
                        if (!is_null($situ_id))
                        {
                            $situ = new Situation();
                            $complaints = $situ->complaints;
                        }
                        else
                        {
                            return $serializer = [
                                'status' => '4',
                                'status_message' => 'KO',
                                'description_message' => 'Situation not found'
                            ];
                        }

                        if ($user)
                        {
                            $complaints = $user->complaints;
                        }
                        else
                        {
                            return $serializer = [
                                'status' => '5',
                                'status_message' => 'KO',
                                'description_message' => 'User not found'
                            ];
                        }
                    }

                    return $serializer = [
                        'status' => '0',
                        'status_message' => 'OK',
                        'complaints' => $complaints
                    ];
                  }
            }
            else
            {
                return $serializer = [
                    'status' => '2',
                    'status_message' => 'KO',
                    'description_message' => 'No Post request',
                ];
            }
        }
        catch (Exception $e)
        {
            return $serializer = [
                'status' => '1',
                'status_message' => 'KO',
                'description_message' => 'KO exception = '.$e,
            ];
        }
    }
}