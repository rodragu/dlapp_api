<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php')
//    require(__DIR__ . '/params.php'),
//    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'rest-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'oauth2' => [
            'class' => 'filsh\yii2\oauth2server\Module',
            'options' => [
                'token_param_name' => 'access_token',
                'access_lifetime' => 3600 * 24
            ],
            'storageMap' => [
                'user_credentials' => 'common\models\User'
            ],
            'grantTypes' => [
                'client_credentials' => [
                    'class' => 'OAuth2\GrantType\ClientCredentials',
                    'allow_public_clients' => false
                ],
                'user_credentials' => [
                    "class" => 'OAuth2\GrantType\UserCredentials'
                ],
                'refresh_token' => [
                    'class' => 'OAuth2\GrantType\RefreshToken',
                    'always_issue_new_refresh_token' => true
                ]
            ],
        ],
    'v1' => [
        'class' => 'rest\versions\v1\RestModule',
        ],
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'loginUrl' => null,
            //'enabledSession' => false,
        ],
        'security' => [
            'class' => 'yii\base\Security',
        ],
        'authmanager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        /*'cache' => [
            'class' => 'yii\redis\Cache',
            'servers' => [
                [
                    'host' => '127.0.0.1',
                    'port' => 6379,
                    'weight' => 100,
                ],
            ],*/
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'class' => '\yii\web\Request',
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                'POST oauth2/<action:\w+>' => 'oauth2/default/<action>',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/complaint', 'v1/user', 'v1/situation', 'oauth2']
                ],
                'OPTIONS v1/user/login' => 'v1/user/login',
                'POST users' => 'v1/user/list',
                'POST login' => 'v1/user/login',
                'POST get_profile' => 'v1/user/get_profile',
                'POST change_password' => 'v1/user/change_password',
                'POST add_user' => 'v1/user/add_user',
                'POST user_update' => 'v1/user/user_update',
                'POST add_complaint' => 'v1/complaint/add_complaint',
                'POST get_complaint' => 'v1/complaint/get_complaint',
                'POST get_complaints' => 'v1/complaint/get_complaints',
                'POST get_user_complaints' => 'v1/complaint/get_user_complaints',
                'POST update_complaint' => 'v1/complaint/update_complaint',
                'POST add_situation' => 'v1/situation/add_situation',
                'POST get_situation' => 'v1/situation/get_situation',
                'POST get_situations' => 'v1/situation/get_situations',
            ],
        ],
    ],
    'params' => $params,
];
