<?php
/**
 * Created by PhpStorm.
 * User: rodrigoaguna
 * Date: 19/11/15
 * Time: 7:41
 */

namespace common\models;

use yii\db\ActiveRecord;

class Complaint extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Complaint}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           [['labor_union_id', 'visit', 'info', 'venue', 'situation_id'], 'safe']
        ];
    }

    public function getSituation()
    {
        return $this->hasOne(Complaint::className(), ['situation_id' => 'id']);
    }

    public function getLabor_union()
    {
        return $this->hasOne(Complaint::className(), ['labor_union_id' => 'id']);
    }
}