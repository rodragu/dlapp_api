<?php
/**
 * Created by PhpStorm.
 * User: rodrigoaguna
 * Date: 19/11/15
 * Time: 7:41
 */

namespace common\models;

use yii\db\ActiveRecord;

class Labor_union extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Labor_union}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    public function getComplaints()
    {
        return $this->hasMany(Complaint::className(), ['labor_union_id' => 'id']);
    }
}