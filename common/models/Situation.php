<?php

namespace common\models;

use yii\db\ActiveRecord;

class Situation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%Situation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }

    public function getComplaints()
    {
        return $this->hasMany(Complaint::className(), ['situation_id' => 'id']);
    }
}